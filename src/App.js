import './App.css';
import { Outlet, Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Link to={'/about'} className="App-link"
          rel="noopener noreferrer">About me</Link>
        <Link to={'/main'} className="App-link"
          rel="noopener noreferrer">Main</Link>
        <a className="App-link"
          href="https:///learn.javascript.ru/"
          target="_blank"
          rel="noopener noreferrer">Learn JS</a>
      </header>
      <main className="App-main">

        <div className='App-main_block'>1</div>
        <div className='App-main_block'>2</div>
        <div className='App-main_block'>3</div>

      </main>

      <footer className="App-footer">
      <a className="App-link"
          href="https://google.com"
          target="_blank"
          rel="noopener noreferrer">Google</a>
        <a
          className="App-link"
          href="https://yandex.ru"
          target="_blank"
          rel="noopener noreferrer">Yandex</a>
        <a
          className="App-link"
          href="https:///learn.javascript.ru/"
          target="_blank"
          rel="noopener noreferrer">Learn JS</a>
      </footer>
    </div>
  );
}

export default App;
