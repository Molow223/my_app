import './main.css';
import { Outlet, Link } from 'react-router-dom';

function Main() {
    return (
        <div className='App'>
            <h1>Hi, people!</h1>
            <Link to={'/'} className="App-link">App</Link>
        </div>
    )
}
export default Main;