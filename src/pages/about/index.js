import './about.css';
import { Outlet, Link } from 'react-router-dom';


function About() {
    return (
        <div className='about'>
            <div className='about_header'>
                <Link to={'/'} className="App-link">App</Link>
                <h1>Страница обо мне</h1>
            </div>

            <div className='about_main'>
                <img src='./img/4adb8a0b0f6214fa7219b2d4c107c79b.jpg'></img>
                <div className='about_description'>
                    <p>
                        {`Привет, меня зовут Максим!
                    Могу создать однастроничное приложение на React! 
                    Это же как с пиццей: на основу из теста(стартовый шаблон),
                    накладывается начинка(сторонние библиотеки)
                    и все это отправляется в печь(сборка проекта)!
                    Приятного аппетита!`}
                    </p>
                    <p>Мои навыки</p>
                    <ul>
                        <li>Html</li>
                        <li>Css</li>
                        <li>JavaScript</li>
                        <li>Figma</li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default About;